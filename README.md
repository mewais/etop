# ETop
ETop is a python based top-like system monitoring and visualization tool that is based on [psutil](https://pypi.python.org/pypi/psutil), [plotly](https://plot.ly/d3-js-for-python-and-pandas-charts/) and [dash](https://plot.ly/products/dash/). You could say that ETop stands for Enhanced Top, but I actually just named after myself (Ewais) because I'm that narcissist.

I wrote this tool to get myself acquainted with plotly and dash because I plan to use them for something else, This means that it was not really tested enough to make sure there're no bugs. There're also some more extensions that I'd like to add but will probably not spend any time on them any soon. However, feel free to submit any issues or requests and I will try to fix/add if I get any free time.

## Dependencies
```bash
sudo pip install dash==0.36.0
sudo pip install dash-html-components==0.13.5
sudo pip install dash-core-components==0.43.0
sudo pip install dash-table==3.1.11
sudo pip install dash-daq==0.1.0
sudo pip install psutil
sudo pip install nvidia-ml-py
sudo pip install netifaces
```

## License
ETop is licensed under LGPLv3. see [LICENSE](./LICENSE)
